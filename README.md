Helios Application

This repository can be placed anywhere on your system. The first time you run it, you will need to set the paths for release directory and the build directory. The release directory should contain any Helios XML repositories provided by Solantro. The build directory will contain the XML files generated from projects that are built with Eclipse. See the Helios user manual for more information.
